package com.magicalcoder.youyaboot.admin.common.config;

import com.magicalcoder.youyaboot.admin.common.util.SpringBootUtil;
import com.magicalcoder.youyaboot.core.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by magicalcoder.com on 2018/8/5.
 * 文件上传 虚拟路径映射 因为springboot默认不支持文件上传 慎重选择
 */
@Slf4j
@Configuration
public class UploadFilePathConfig extends WebMvcConfigurerAdapter {
    /*上传目录*/
    @Value("${magicalcoder.file.upload.mapping.uploadDiskFolder:}")
    private String uploadDiskFolder;
    /*虚拟前缀*/
    @Value("${magicalcoder.file.upload.mapping.requestPrefix:}")
    private String requestPrefix;
    /*文件上传额外添加的前缀*/
    @Value("${magicalcoder.file.upload.mapping.fileExtraAddPrefix:}")
    private String fileExtraAddPrefix;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        uploadDiskFolder = lastUploadPath(uploadDiskFolder);
        if(!requestPrefix.startsWith("/")){
            requestPrefix = "/"+requestPrefix;
        }
        if(!requestPrefix.endsWith("/")){
            requestPrefix = requestPrefix+"/";
        }
        log.info("文件映射路径"+uploadDiskFolder);
        registry.addResourceHandler(requestPrefix+"**").addResourceLocations("file:" + uploadDiskFolder);
    }

    private String lastUploadPath(String uploadDiskFolder){
        if(StringUtil.isBlank(uploadDiskFolder)){
            uploadDiskFolder = SpringBootUtil.getJarDirPath();
            log.info("jar file path:"+uploadDiskFolder);
//            uploadDiskFolder = UploadFilePathConfig.class.getResource("/").getPath();
            if(!uploadDiskFolder.equals("\\")){
                uploadDiskFolder +="\\";
            }
            uploadDiskFolder +="upload\\";
        }
        uploadDiskFolder = uploadDiskFolder.replaceAll("\\\\","/");
        if(!uploadDiskFolder.startsWith("/")){
            uploadDiskFolder = "/"+uploadDiskFolder;
        }
        if(!uploadDiskFolder.endsWith("/")){
            uploadDiskFolder = uploadDiskFolder+"/";
        }
        return uploadDiskFolder;
    }

    public String getUploadDiskFolder() {
        return uploadDiskFolder;
    }

    public String getRequestPrefix() {
        return deleteBeginEndSplit(requestPrefix);
    }

    public String getFileExtraAddPrefix() {
        return fileExtraAddPrefix;
    }


    private String deleteBeginEndSplit(String prefix ){
        if(prefix.startsWith("/")){
            prefix = prefix.replaceFirst("/","");
        }
        if(prefix.endsWith("/")){
            prefix = prefix.substring(0,prefix.length()-1);
        }
        return prefix;
    }

}
